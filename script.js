var Token = function (kind, value) {
    this.kind = kind;
    if (value) {
        this.value = value;
    }
};

var TokenStream = function () {
    this._expression = undefined;
};

TokenStream.prototype = {
    getToken: function () {
        if (this._full) {
            this._full = false;
            return this._buffer;
        }
        var ch = this._expression.shift();
        switch (ch) {
            case undefined:
                return false;
            case '(':
            case ')':
            case '+':
            case '-':
            case '*':
            case '/':
                return new Token(ch);
            case '.':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this._expression.unshift(ch);
                var val = this._readNumber();
                return new Token('8', val);
            default:
                throw new Error('Wrong lexeme: ' + ch);
        }
    },
    putback : function (token) {
        if (this._full) {
            throw new Error('putback() in full buffer');
        }
        this._buffer = token;
        this._full = true;
    },
    setExpression: function (expression) {
        this._expression = expression;
    },
    getExpression: function () {
        return this._expression;
    },
    _readNumber: function () {
        var num = '';
        var ch = this._expression.shift();
        while (ch) {
            switch (ch) {
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    num += ch;
                    ch = this._expression.shift();
                    break;
                default:
                    this._expression.unshift(ch);
                    return Number(num);
            }
        }
        return Number(num);
    },
    _full: false,
    _buffer: undefined
};

var Grammar = function () {
    this.ts = new TokenStream();
};

Grammar.prototype = {
    expression: function () {
        var left = this.term();
        var token = this.ts.getToken();
        while (true) {
            if (token) {
                switch (token.kind) {
                    case '+':
                        left += this.term();
                        token = this.ts.getToken();
                        break;
                    case '-':
                        left -= this.term();
                        token = this.ts.getToken();
                        break;
                    default:
                        this.ts.putback(token);
                        return left;
                }
            } else {
                return left;
            }
        }
    },
    term: function () {
        var left = this.primary();
        var token = this.ts.getToken();
        while (true) {
            switch (token.kind) {
                case '*':
                    left *= this.primary();
                    token = this.ts.getToken();
                    break;
                case '/':
                    var d = this.primary();
                    if (d === 0) {
                        throw new Error('Division by zero');
                    }
                    left /= d;
                    token = this.ts.getToken();
                    break;
                default:
                    this.ts.putback(token);
                    return left;
            }
        }
    },
    primary: function () {
        var token = this.ts.getToken();
        switch (token.kind) {
            case '(':
                var expression = this.expression();
                token = this.ts.getToken();
                if (token.kind !== ')') {
                    throw new Error('")" expected');
                }
                return expression;
            case '8':
                return token.value;
            default:
                throw new Error('Expected primary expression, got: ' + token.kind);
        }
    }
};

var Calculator = function () {
    Grammar.call(this);
};
Calculator.prototype = Object.create(Grammar.prototype);
Calculator.prototype.constructor = Calculator;

Calculator.prototype.calculate = function (expression) {
    this.ts.setExpression(expression.split(''));
    while (this.ts.getExpression().length !== 0) {
        var token = this.ts.getToken();
        this.ts.putback(token);
        console.log(this.expression());
    }
};